<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_config.php';

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {
    
    //post of hashtag
    //like of hashtag
    //follow of user
    //like of user


    //like everything from a hashtag feed
    $queryHashtag = 'motorcycle';
    $limit = rand(45, 51);
    $counter = 0;
    $maxId = null;
    do {
        $response = $ig->hashtag->getFeed($queryHashtag, $maxId);

        foreach ($response->getItems() as $item) {
            printf("https://instagram.com/p/%s/\n", $item->getCode());

            $module = 'feed_contextual_hashtag';
            $extraData = [$queryHashtag => $hashtag];

            if ($counter < $limit) {
                //first activation
                if ($counter == 0) {
                    $sleepFor = rand(2, 5);
                    sleep($sleepFor);
                }
                $skip = rand(1, 10);
                if ($skip <= 10 - $skipPercentage) {
                    $ig->media->like($item->getId(), $module, $extraData);
                }

                $counter += 1;
                $seconds = rand($sleepLoopLow, $sleepLoopHigh);
                sleep($seconds);
            } else {
                break;
            }
        }

        if ($counter == $limit) {
            break;
        }
        $maxId = $response->getNextMaxId();

        echo "Sleeping for 5s...\n";
        sleep(5);
    } while ($maxId !== null);



  
    //like everything from a user
    // $queryUserName = 'tobiaskaufmann';
    // $queryUserId = $ig->people->getUserIdForName($queryUserName);
    // $maxId = null;
    // do {
    //     $response = $ig->timeline->getUserFeed($queryUserId, $maxId);

    //     foreach ($response->getItems() as $item) {
    //         printf("[%s] https://instagram.com/p/%s/\n", $item->getId(), $item->getCode());

    //         $module = 'photo_view_profile';
    //         $extraData = [$queryUserName => $username, $queryUserId => $userId];
    //         $ig->media->like($item->getId(), $module, $extraData);
    //     }
    //     $maxId = $response->getNextMaxId();

    //     echo "Sleeping for 5s...\n";
    //     sleep(5);
    // } while ($maxId !== null);



} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}

?>