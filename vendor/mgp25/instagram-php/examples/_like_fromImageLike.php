<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_db.php';
require './_config.php';

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {

    //like last because user liked last image from a specific user
    $queryUserName = 'motorcyclelife';
    $queryUserId = $ig->people->getUserIdForName($queryUserName);
    $limit = rand(7, 11);
    $counter = 0;
    $maxId = null;
    do {
        $response = $ig->timeline->getUserFeed($queryUserId, $maxId);

        foreach ($response->getItems() as $item) {
            printf("image: https://instagram.com/p/%s/\n", $item->getCode());

            $likersResponse = $ig->media->getLikers($item->getId());

            foreach ($likersResponse->getUsers() as $user) {

                if ($counter < $limit) {
                    $userData = get_object_vars($user);
                    $response2 = $ig->timeline->getUserFeed($userData['pk']);

                    $skip = rand(1, 10);
                    if ($skip <= 10 - $skipPercentage) {
                        foreach ($response2->getItems() as $targetItem) {
                        
                            // first activation
                            if ($counter == 0) {
                                $sleepFor = rand(2, 5);
                                sleep($sleepFor);
                            }

                            $module = 'photo_view_profile';
                            $extraData = [$queryHashtag => $hashtag];
                            $ig->media->like($targetItem->getId(), $module, $extraData);
                            echo 'image: ' . 'https://instagram.com/p/' . $targetItem->getCode() . '  liked user: ' . $userData['username'] . '<br>';
                            $counter += 1;
                            $seconds = rand($sleepLoopLow, $sleepLoopHigh);
                            sleep($seconds);
                            break;
                        }
                    }

                } else {
                    break;
                }
            }
            break;
        }
        break;

        $maxId = $response->getNextMaxId();

        echo "Sleeping for 5s...\n";
        sleep(5);
    } while ($maxId !== null);



    // //follow because user liked last image from a specific user
    // $targetUsername;

    // $queryHashtag = 'motorcycle';  
    // // $limit = rand(54, 61);
    // $limit = 4;
    // $counter = 0;
    // $maxId = null;
    // do {
    //     $response = $ig->media->g($queryHashtag, $maxId);

    //     foreach ($response->getItems() as $item) {

    //         $res = $ig->media->getInfo($item->getId());
    //         $mediaItems = $res->getItems();

    //         $newArray = get_object_vars($mediaItems['0']);
    //         // echo '<pre>' . var_export($newArray, true) . '</pre>';   
    //         $newArray2 = get_object_vars($newArray['caption']);
    //         // echo '<pre>' . var_export($newArray2, true) . '</pre>';   
    //         $userId = $newArray2['user_id'];

    //         if ($counter < $limit) {
    //             try {
    //                 $stmt = $db->prepare("SELECT * FROM following WHERE user_id=:user_id");
    //                 $stmt->execute(array(':user_id' => $userId));
    //                 $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //                 if (count($data) == 0) {
    //                     // first activation
    //                     if ($counter == 0) {
    //                         $sleepFor = rand(2, 5);
    //                         sleep($sleepFor);
    //                     }

    //                     $ig->people->follow($userId);
    //                     $stmt = $db->prepare("INSERT INTO following(username, user_id, date_following, following) VALUES(:username, :user_id, :date_following, :following)");
    //                     $stmt->execute(array(':username' => $value, ':user_id' => $userId, ':date_following' => date("Y-m-d H:i:s"), ':following' => true));

    //                     echo 'followed ' . $value . '<br>';
    //                     $counter += 1;
    //                     $seconds = rand(7, 13);
    //                     // sleep($seconds);
    //                 }
    //             } catch (PDOException $ex) {
    //                 echo "An Error occured!";
    //                 printf($ex->getMessage());
    //             }
    //         } else {
    //             break;
    //         }
    //     }
    //     if ($counter == $limit) {
    //         break;
    //     }
    //     $maxId = $response->getNextMaxId();

    //     echo "Sleeping for 5s...\n";
    //     sleep(5);
    // } while ($maxId !== null);


} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}

?>