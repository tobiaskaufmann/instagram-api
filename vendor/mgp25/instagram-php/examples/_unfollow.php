<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_db.php';
require './_config.php';

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {
    $limit = rand(54, 61);
    $counter = 0;

    // $userId = $ig->people->getUserIdForName('bikezoned');
    // $response = $ig->people->getFollowing($userId);
    // $followers = $response->getUsers();
    // $usernames = array_column($followers, 'username');
    // print_r($usernames);

    try {
        // SELECT * FROM following WHERE date_following < DATE_ADD(NOW(), INTERVAL -1 DAY)
        $stmt = $db->prepare("SELECT * FROM following WHERE following = :following AND date_following < DATE_ADD(NOW(), INTERVAL :day_diff DAY)");
        $stmt->execute(array(':following' => true, ':day_diff' => -2));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($data as $user) {
            echo 'loop<br>';
            if ($counter < $limit) {
                if ($counter == 0) {
                    $sleepFor = rand(2, 5);
                    sleep($sleepFor);
                }

                $skip = rand(1, 10);
                if ($skip <= 10 - $skipPercentage) {
                    $stmt = $db->prepare("UPDATE following SET following=? WHERE user_id=?");
                    $stmt->execute(array(false, $user['user_id']));

                    $ig->people->unfollow($user['user_id']);
                    echo 'unfollowed ' . $user['username'] . '<br>';
                }

                $counter += 1;
                $seconds = rand($sleepLoopLow, $sleepLoopHigh);
                sleep($seconds);

            } else {
                break;
            }
        }
    } catch (PDOException $ex) {
        echo "An Error occured!";
        printf($ex->getMessage());
    }

} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}

?>