<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_db.php';
require './_config.php';


$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {

    //-follow because user posted picture with a specific hashtag
    //-follow because user is following a specific user
    //follow because user liked picture from hashtag search
    //follow because user liked picture from a specific user

    
    //like everything from a user (not working)
    // $queryUserName = 'bikezoned';
    // $queryUserId = $ig->people->getUserIdForName($queryUserName);
    // $maxId = null;
    // do {
    //     $response = $ig->timeline->getUserFeed($queryUserId, $maxId);

    //     foreach ($response->getItems() as $item) {

    //         $response2 = $ig->media->getLikers($item->getId());
    //         foreach ($response2->getUsers() as $user) {
    //             // echo 'lol ' . $user . '<br>';
    //             var_export($user,true);
    //             echo 'loop' . '<br>';
                
    //         }
    //     }
    //     $maxId = $response->getNextMaxId();

    //     echo "Sleeping for 5s...\n";
    //     sleep(5);
    // } while ($maxId !== null);

    //follow because user is following a specific user
    $queryUserId = $ig->people->getUserIdForName('motorcyclelife');
    $response = $ig->people->getFollowers($queryUserId);
    $followers = $response->getUsers();
    $usernames = array_column($followers, 'username');

    $limit = rand(7, 11);
    $counter = 0;

    foreach ($usernames as $value) {
        $userId = $ig->people->getUserIdForName($value);

        if ($counter < $limit) {

            try {
                $stmt = $db->prepare("SELECT * FROM following WHERE user_id=:user_id");
                $stmt->execute(array(':user_id' => $userId));
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($data) == 0) {
                    //first activation
                    if ($counter == 0) {
                        $sleepFor = rand(2, 5);
                        sleep($sleepFor);
                    }

                    $skip = rand(1, 10);
                    if ($skip <= 10 - $skipPercentage) {
                        $ig->people->follow($userId);
                        $stmt = $db->prepare("INSERT INTO following(username, user_id, date_following, following) VALUES(:username, :user_id, :date_following, :following)");
                        $stmt->execute(array(':username' => $value, ':user_id' => $userId, ':date_following' => date("Y-m-d H:i:s"), ':following' => true));

                        echo 'followed ' . $value . '<br>';
                    }

                    $counter += 1;
                    $seconds = rand($sleepLoopLow, $sleepLoopHigh);
                    sleep($seconds);
                }

            } catch (PDOException $ex) {
                echo "An Error occured!";
                printf($ex->getMessage());
            }
        } else {
            break;
        }

    }


    //follow because user posted picture with a specific hashtag
    // $queryHashtag = 'motorcycle';
    // $maxId = null;
    // do {
    //     $response = $ig->hashtag->getFeed($queryHashtag, $maxId);

    //     foreach ($response->getItems() as $item) {

    //         $res = $ig->media->getInfo($item->getId());
    //         $mediaItems = $res->getItems();

    //         // $keys = array_keys($mediaItems);

    //         $newArray = get_object_vars($mediaItems['0']);
    //         // echo '<pre>' . var_export($newArray, true) . '</pre>';   
    //         $newArray2 = get_object_vars($newArray['caption']);
    //         // echo '<pre>' . var_export($newArray2, true) . '</pre>';   

    //         $ig->people->follow($newArray2['user_id']);
    //         echo 'followed: ' . $newArray2['user_id']  . '<br>';
    //     }
    //     $maxId = $response->getNextMaxId();

    //     echo "Sleeping for 5s...\n";
    //     sleep(5);
    // } while ($maxId !== null);


} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}

?>