<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_db.php';
require './_config.php';

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {
    
    //post of hashtag
    //like of hashtag
    //follow of user
    //like of user



    //comment everything from a hashtag feed
    $queryHashtag = 'motorcycle';
    $limit = rand(15, 21);
    $counter = 0;
    $maxId = null;
    do {
        $response = $ig->hashtag->getFeed($queryHashtag, $maxId);

        foreach ($response->getItems() as $item) {
            printf("https://instagram.com/p/%s/\n", $item->getCode());

            try {
                $stmt = $db->query("SELECT * FROM comment_template");
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $module = 'feed_contextual_hashtag';
                $extraData = [$queryHashtag => $hashtag];

                $index = rand(0, count($data));
                $text = $data[$index]['text'];

                if ($counter < $limit) {
                    //first activation
                    if ($counter == 0) {
                        $sleepFor = rand(2, 5);
                        sleep($sleepFor);
                    }

                    $skip = rand(1, 10);
                    if ($skip <= 10 - $skipPercentage) {
                        // $ig->media->like($item->getId(), $module, $extraData);
                        $ig->media->comment($item->getId(), $text, null, 'comments_feed_timeline');
                    }

                    $counter += 1;
                    $seconds = rand(15, 20);
                    sleep($seconds);
                } else {
                    break;
                }

            } catch (PDOException $ex) {
                echo "An Error occured!";
                printf($ex->getMessage());
            }
        }
        if ($counter == $limit) {
            break;
        }
        $maxId = $response->getNextMaxId();

        echo "Sleeping for 5s...\n";
        sleep(5);
    } while ($maxId !== null);



  
    //comment everything from a user
    // $queryUserName = 'tobiaskaufmann';
    // $queryUserId = $ig->people->getUserIdForName($queryUserName);
    // $maxId = null;
    // do {
    //     $response = $ig->timeline->getUserFeed($queryUserId, $maxId);

    //     foreach ($response->getItems() as $item) {
    //         printf("[%s] https://instagram.com/p/%s/\n", $item->getId(), $item->getCode());

    //         $module = 'photo_view_profile';
    //         $extraData = [$queryUserName => $username, $queryUserId => $userId];
    //         $ig->media->like($item->getId(), $module, $extraData);
    //     }
    //     $maxId = $response->getNextMaxId();

    //     echo "Sleeping for 5s...\n";
    //     sleep(5);
    // } while ($maxId !== null);



} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}

?>