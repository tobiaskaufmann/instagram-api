<?php
set_time_limit(0);
date_default_timezone_set('UTC');
require __DIR__ . '/../../../autoload.php';
require './_db.php';
require './_config.php';

$ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
    exit(0);
}

try {

    try {
        $stmt = $db->prepare("SELECT * FROM image WHERE posted=:posted ORDER BY ID LIMIT 1");
        $stmt->execute(array(':posted' => false));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt2 = $db->query("SELECT * FROM signature");
        $signature = $stmt2->fetchAll(PDO::FETCH_ASSOC);

        if (count($data) == 1 && count($signature) == 1) {
            sleep(2, 5);
            $imageId = $data[0]['ID'];

            $videoFilename = './assets/' . $data[0]['filename'];
            $captionText = $data[0]['text'] . $signature[0]['text'];
            $ig->timeline->uploadVideo($videoFilename, ['caption' => $captionText]);

            $stmt3 = $db->prepare("UPDATE image SET posted=? WHERE id=?");
            $stmt3->execute(array(true, $imageId));
        }

    } catch (PDOException $ex) {
        echo "An Error occured!";
        printf($ex->getMessage());
    }
} catch (\Exception $e) {
    echo 'Something went wrong: ' . $e->getMessage() . "\n";
}
